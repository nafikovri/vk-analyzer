﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201512091416)]
	public class Mig012_CreateTable_LoginUser_User : Migration
	{
		private const string TableName = "LoginUser_User";

		public override void Up()
		{
			var keyColumns = new[] {"loginUserId", "userId"};

			Create.Table(TableName)
			      .WithColumn(keyColumns[0]).AsInt64().NotNullable()
			      .WithColumn(keyColumns[1]).AsInt64().NotNullable();

			Create.PrimaryKey("PK_" + TableName)
			      .OnTable(TableName)
			      .Columns(keyColumns);
		}

		public override void Down()
		{
			Delete.PrimaryKey("PK_" + TableName)
			      .FromTable(TableName);

			Delete.Table(TableName);
		}
	}
}

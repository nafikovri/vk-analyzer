﻿
using Newtonsoft.Json;

namespace VkApi.Entities
{
	public class CommentsInfo
	{
		[JsonProperty("count")]
		public uint Count { get; set; }

		[JsonProperty("can_post")]
		public bool CanUserPost { get; set; }
	}
}

﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201511291537)]
	public class Mig007_AlterTableUser : Migration
	{
		public override void Up()
		{
			Rename.Column("birthdate").OnTable("User").InSchema("vkanalyzer").To("birthday");
		}

		public override void Down()
		{
			Rename.Column("birthday").OnTable("User").InSchema("vkanalyzer").To("birthdate");
		}
	}
}

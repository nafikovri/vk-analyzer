﻿
using Domain.Domains;

using FluentNHibernate.Mapping;

namespace Domain.Maps
{
	public class PostSnapshotMap : ClassMap<PostSnapshotDomain>
	{
		public PostSnapshotMap()
		{
			Table("postsnapshot");

			Id(x => x.Id).Column("id").GeneratedBy.Increment();
			References(x => x.Post).Column("postId").Not.Nullable();
			Map(x => x.Date).Column("date");
			Map(x => x.CountComments).Column("countComments");
			Map(x => x.CountLikes).Column("countLikes");
			Map(x => x.CountReposts).Column("countReposts");
		}
	}
}

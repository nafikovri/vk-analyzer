﻿
using AspNet.Identity;

using AutoMapper;

using Domain;

using VkApi.Entities;

namespace VkAnalyzer.Web.Infrastructure
{
	public class AutoMapping
	{
		public static void RegisterMappings()
		{
			GlobalAutoMapping.RegisterMappings();

			Mapper.CreateMap<User, IdentityUser>();
		}
	}
}
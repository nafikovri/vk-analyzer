﻿
using FluentMigrator;

namespace DatabaseMigrations.Schema
{
	[Migration(201511252151)]
	public class Mig002_CreateTableCountry : Migration
	{
		private const string TableName = "Country";

		public override void Up()
		{
			Create.Table(TableName)
			      .WithColumn("id").AsInt32().NotNullable()
			      .WithColumn("title").AsString(30).NotNullable();

			Create.PrimaryKey("PK_" + TableName).OnTable(TableName).Column("id");
		}

		public override void Down()
		{
			Delete.Table(TableName);
		}
	}
}

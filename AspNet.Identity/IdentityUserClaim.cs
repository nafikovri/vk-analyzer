﻿
using FluentNHibernate.Mapping;

namespace AspNet.Identity
{
	public class IdentityUserClaim
	{
		public virtual int Id { get; set; }
		public virtual string Type { get; set; }
		public virtual string Value { get; set; }
		public virtual IdentityUser User { get; set; }
	}

	public class IdentityUserClaimMap : ClassMap<IdentityUserClaim>
	{
		public IdentityUserClaimMap()
		{
			Table("claim");

			Id(x => x.Id).Column("id");
			Map(x => x.Type).Column("type");
			Map(x => x.Value).Column("value");

			References(x => x.User).Column("userId");
		}
	}
}

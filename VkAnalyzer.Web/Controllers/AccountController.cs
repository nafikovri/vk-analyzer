﻿
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

using AspNet.Identity;

using AutoMapper;

using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;

using VkAnalyzer.Web.Infrastructure;
using VkAnalyzer.Web.Models;

using VkApi;

namespace VkAnalyzer.Web.Controllers
{
	[Authorize]
	public class AccountController : Controller
	{
		private const int ClientId = 5180758;
		private const string ClientSecret = "VsuAJy19PH7atg2cPRFp";
		private const string Scope = "email,friends,wall,offline";

		private UserManager<IdentityUser> UserManager { get; set; }

		private IAuthenticationManager AuthenticationManager
		{
			get
			{
				return this.HttpContext.GetOwinContext().Authentication;
			}
		}

		public AccountController(UserManager<IdentityUser> userManager)
		{
			UserManager = userManager;
		}

		[AllowAnonymous]
		public ActionResult Login(string returnUrl)
		{
			ViewBag.ReturnUrl = returnUrl;
			return View();
		}

		[AllowAnonymous]
		public RedirectResult RedirectToVk()
		{
			return Redirect(
				string.Format(
					"https://oauth.vk.com/authorize?client_id={0}&redirect_uri={1}&display=page&scope={2}&v=5.37",
					ClientId,
					string.Concat(GetRootUrl(), Url.Action("AuthVk")),
					Scope));
		}

		[AllowAnonymous]
		public async Task<ActionResult> AuthVk(string code, string error, string error_description)
		{
			if (!code.IsNullOrWhiteSpace())
			{
				var identityUser = await GetIdentityUserByCode(code);

				var existingUser = await UserManager.FindByIdAsync(identityUser.Id); // пытаемся найти такого пользователя
				if (existingUser == null)
				{
					// пользователь зашел первый раз.
					// todo: предложить ввести логин
					/*
					ViewBag.IdentityUser = identityUser;
					ViewBag.LoginProvider = "ВКонтакте";

					return JavaScript(string.Format("window.open('{0}{1}')", this.GetRootUrl(), Url.Action("ExternalLoginConfirmation")));
					//*/

					if (!await Registration(identityUser))
					{
						return View("FailedVkAuth");
					}
				}

				// собственно авторизация
				await SignInAsync(identityUser, true);
				return RedirectToLocal(ViewBag.ReturnUrl);
			}

			// что-то пошло не так
			// или пользователь отказался
			return View("FailedVkAuth");
		}

		[AllowAnonymous]
		public ActionResult ExternalLoginConfirmation()
		{
			return this.View("ExternalLoginConfirmation");
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public ActionResult ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model)
		{
			if (User.Identity.IsAuthenticated)
				return RedirectToLocal(ViewBag.ReturnUrl);

			if (ModelState.IsValid)
			{
				return RedirectToLocal(ViewBag.ReturnUrl);
			}

			return RedirectToLocal(ViewBag.ReturnUrl);
		}

		private async Task<bool> Registration(IdentityUser identityUser)
		{
			var result = await this.UserManager.CreateAsync(identityUser);
			return result.Succeeded;
		}

		/// <summary>
		/// Перенаправляет на заданный Url, если он принадлежит этому приложению.
		/// Иначе перенаправляет на главную.
		/// </summary>
		/// <param name="returnUrl">Url, на который предполагается перенаправление</param>
		/// <returns></returns>
		private ActionResult RedirectToLocal(string returnUrl)
		{
			if (returnUrl != null &&
			    Url.IsLocalUrl(returnUrl))
			{
				return Redirect(returnUrl);
			}

			return RedirectToAction("Index", "Home");
		}

		private async Task<bool> HasPassword()
		{
			return await UserManager.HasPasswordAsync(User.Identity.GetUserId());
		}

		/// <summary>
		/// Авторизует пользователя.
		/// </summary>
		/// <param name="user">Собственно пользователь</param>
		/// <param name="isPersistent">"Запомнить меня"</param>
		/// <returns></returns>
		private async Task SignInAsync(IdentityUser user, bool isPersistent)
		{
			AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
			
			var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
			
			AuthenticationManager.SignIn(
				new AuthenticationProperties() {IsPersistent = isPersistent},
				identity);
		}

		private string GetRootUrl()
		{
			return string.Format("{0}://{1}", Request.Url.Scheme, Request.Url.Authority);
		}

		private async Task<IdentityUser> GetIdentityUserByCode(string code)
		{
			// получить токен
			var vkApi = IoC.Container.Resolve<IVkApi>();
			var accessToken = await vkApi.GetAccessTokenAsync(
				ClientId,
				ClientSecret,
				string.Format("{0}{1}", this.GetRootUrl(), Url.Action("AuthVk")),
				code);

			var user = await vkApi.GetUserAsync(accessToken.UserId);

			var identityUser = Mapper.Map<IdentityUser>(user);
			identityUser.AccessToken = accessToken.Token;
			//identityUser.UserName = accessToken.Email;
			identityUser.UserName = identityUser.Id;					// todo: временно. сделать запрос Username у пользователя

			return identityUser;
		}

	}
}
﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201512121854)]
	public class Mig019_CreateTableClaim : Migration
	{
		private const string TableName = "Claim";

		public override void Up()
		{
			Create.Table(TableName)
			      .WithColumn("id").AsInt32().PrimaryKey()
			      .WithColumn("type").AsString().NotNullable()
			      .WithColumn("value").AsString().NotNullable()
				  .WithColumn("userId").AsInt64().NotNullable();

			Create.ForeignKey("FK_Claim_LoginUser")
			      .FromTable("Claim")
			      .ForeignColumn("userId")
			      .ToTable("LoginUser")
			      .PrimaryColumn("id");
		}

		public override void Down()
		{
			Delete.ForeignKey("FK_Claim_LoginUser")
			    .OnTable("Claim");

			Delete.Table(TableName);
		}
	}
}

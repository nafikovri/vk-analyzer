﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using NHibernate;
using NHibernate.Criterion;
using NHibernate.Linq;

namespace DatabaseAccess.Repositories
{
	public class Repository<T> : IRepository<T> where T : class
	{
		private readonly ISession _session;

		public Repository(ISession session)
		{
			_session = session;
		}


		#region IReadOnlyRepository<T>

		public T FindBy(object id)
		{
			return _session.Get<T>(id);
		}

		public IQueryable<T> GetAll()
		{
			return _session.Query<T>();
		}

		public T FindBy(Expression<Func<T, bool>> expression)
		{
			return this.FilterBy(expression).SingleOrDefault();
		}

		public IQueryable<T> FilterBy(Expression<Func<T, bool>> expression)
		{
			return this.GetAll().Where(expression);
		}

		public IList<T> FilterBy(ICriterion criterion)
		{
			var query = _session.CreateCriteria(typeof(T));
			query.Add(criterion);
			return query.List<T>();
		}

		public IList<T> FilterBy(IEnumerable<ICriterion> criteria)
		{
			var query = _session.CreateCriteria(typeof(T));

			foreach (var criterion in criteria)
				query.Add(criterion);

			return query.List<T>();
		}


		public IQueryOver<T, T> QueryOver()
		{
			return _session.QueryOver<T>();
		}
		public IQueryOver<T, T> QueryOver(Expression<Func<T>> alias)
		{
			return _session.QueryOver(alias);
		}

		#endregion


		#region IRepository<T>

		public void Save(T entity)
		{
			ActionInTransaction(
				() =>
				_session.Save(entity));
		}

		public void Save(IEnumerable<T> items)
		{
			ActionInTransaction(
				() =>
				{
					foreach (var item in items)
						_session.Save(item);
				});
		}

		public void SaveOrUpdate(T entity)
		{
			ActionInTransaction(
				() =>
				_session.SaveOrUpdate(entity));
		}

		public void SaveOrUpdate(IEnumerable<T> items)
		{
			ActionInTransaction(
				() =>
				{
					foreach (var item in items)
						_session.SaveOrUpdate(item);
				});
		}

		public void Update(T entity)
		{
			ActionInTransaction(
				() =>
				_session.Update(entity));
		}

		public void Update(IEnumerable<T> items)
		{
			ActionInTransaction(
				() =>
				{
					foreach (var item in items)
						_session.Update(item);
				});
		}

		public void Merge(T entity)
		{
			this.ActionInTransaction(
				() =>
				this._session.Merge(entity));
		}

		public void Merge(IEnumerable<T> items)
		{
			ActionInTransaction(
				() =>
				{
					foreach (var item in items)
						_session.Merge(item);
				});
		}

		public void Delete(T entity)
		{
			ActionInTransaction(
				() =>
				_session.Delete(entity));
		}

		public void Delete(IEnumerable<T> items)
		{
			ActionInTransaction(
				() =>
				{
					foreach (var item in items)
						_session.Delete(item);
				});
		}

		#endregion

		private void ActionInTransaction(Action action)
		{
			using (var transaction = _session.BeginTransaction())
			{
				action();
				transaction.Commit();
			}
		}

	}
}

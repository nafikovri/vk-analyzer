﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201601080358)]
	public class Mig020_DeleteForeignKey_LoginUser_Post : Migration
	{
		public override void Up()
		{
			Delete.ForeignKey("FK_LoginUser_Post_LoginUser")
				  .OnTable("LoginUser_Post");

			Delete.ForeignKey("FK_LoginUser_Post_Post")
				  .OnTable("LoginUser_Post");
		}

		public override void Down()
		{
			Create.ForeignKey("FK_LoginUser_Post_LoginUser")
				  .FromTable("LoginUser_Post")
				  .ForeignColumn("loginUserId")
				  .ToTable("LoginUser")
				  .PrimaryColumn("id");

			Create.ForeignKey("FK_LoginUser_Post_Post")
				  .FromTable("LoginUser_Post")
				  .ForeignColumn("postId")
				  .ToTable("Post")
				  .PrimaryColumn("id");
		}
	}
}

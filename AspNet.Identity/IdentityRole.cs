﻿
using System.Collections.Generic;

using FluentNHibernate.Mapping;

using Microsoft.AspNet.Identity;

namespace AspNet.Identity
{
	public class IdentityRole : IRole
	{
		public virtual string Id
		{
			get { return DbId.ToString(); }
			protected set { DbId = int.Parse(value); }
		}
		public virtual int DbId { get; protected set; }

		public virtual string Name { get; set; }

		public virtual IList<IdentityUser> Users { get; protected set; }

		public IdentityRole()
		{
			Users = new List<IdentityUser>();
		}

		public IdentityRole(string roleName)
			: this()
		{
			Name = roleName;
		}
	}

	public class IdentityRoleMap : ClassMap<IdentityRole>
	{
		public IdentityRoleMap()
		{
			this.Table("role");

			this.Id(x => x.DbId).Column("id").GeneratedBy.Increment();
			Map(x => x.Name).Column("name");

			HasManyToMany(x => x.Users)
				.Table("loginuser_role")
				.ParentKeyColumn("roleId")
				.ChildKeyColumn("loginUserId")
				.LazyLoad().Cascade.SaveUpdate();
		}
	}
}

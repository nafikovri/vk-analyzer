﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201512121846)]
	public class Mig018_CreateTablesWithRole : Migration
	{
		public override void Up()
		{
			Create.Table("Role")
			      .WithColumn("id").AsInt32().PrimaryKey()
			      .WithColumn("name").AsString().NotNullable().Unique();

			Create.Table("LoginUser_Role")
			      .WithColumn("loginUserId").AsInt64().NotNullable()
				  .WithColumn("roleId").AsInt32().NotNullable();

			Create.ForeignKey("FK_LoginUser_Role_Role")
			      .FromTable("LoginUser_Role")
			      .ForeignColumn("roleId")
			      .ToTable("Role")
			      .PrimaryColumn("id");

			Create.ForeignKey("FK_LoginUser_Role_LoginUser")
				  .FromTable("LoginUser_Role")
				  .ForeignColumn("loginUserId")
				  .ToTable("LoginUser")
				  .PrimaryColumn("id");
		}

		public override void Down()
		{
			Delete.ForeignKey("FK_LoginUser_Role_LoginUser")
			      .OnTable("LoginUser_Role");
			Delete.ForeignKey("FK_LoginUser_Role_Role")
				  .OnTable("LoginUser_Role");

			Delete.Table("LoginUser_Role");
			Delete.Table("Role");
		}
	}
}

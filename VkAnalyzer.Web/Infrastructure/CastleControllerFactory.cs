﻿
using System;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml;

using Castle.Windsor;

namespace VkAnalyzer.Web.Infrastructure
{
	public class CastleControllerFactory : DefaultControllerFactory
	{
		public IWindsorContainer Container { get; protected set; }

		public CastleControllerFactory(IWindsorContainer container)
		{
			if (container == null)
			{
				throw new ArgumentNullException("container");
			}

			Container = container;
		}

		protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
		{
			if (controllerType == null)
				return null;

			return this.Container.Resolve(controllerType) as IController;
		}

		public override void ReleaseController(IController controller)
		{
			var diposableController = controller as IDisposable;
			if (diposableController != null)
				diposableController.Dispose();

			Container.Release(controller);
		}
	}
}
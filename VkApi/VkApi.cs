﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using VkApi.Entities;
using VkApi.Response;

namespace VkApi
{
	public class VkApi : IVkApi
	{
		private const string Url = "https://api.vk.com/method/{0}?{1}&v=5.37";

		private readonly JsonDownloader _jsonDownloader;

		public string AccessToken { get; set; }


		public VkApi(JsonDownloader jsonDownloader)
		{
			this._jsonDownloader = jsonDownloader;
		}

		public async Task<User> GetUserAsync(uint id, string extraOptions = null)
		{
			var response = await _jsonDownloader.DownloadAsync<List<User>>(
				String.Format(
					Url,
					"users.get",
					String.Format("user_ids={0}&{1}", id, extraOptions)),
				AccessToken
				);

			if (response != null &&
			    response.Response != null &&
			    response.Response.Count != 0)
				return response.Response[0];

			return null;
		}

		public async Task<List<User>> GetUsersAsync(uint[] ids, string extraOptions = null)
		{
			var response = await _jsonDownloader.DownloadAsync<List<User>>(
				String.Format(
					Url,
					"users.get",
					String.Format("user_ids={0}&{1}", string.Join(",", ids), extraOptions)),
				AccessToken
				);

			if (response != null &&
			    response.Response != null)
				return response.Response;

			return new List<User>();
		}

		public async Task<ResolveResult> ResolveScreenNameAsync(string screenName)
		{
			var response = await _jsonDownloader.DownloadAsync<ResolveResult>(
				String.Format(
					Url,
					"utils.resolveScreenName",
					String.Format("screen_name={0}", screenName)));

			if (response != null)
				return response.Response;

			return null;
		}

		public async Task<List<User>> GetFriendsAsync(uint id, string extraOptions = null)
		{
			if (string.IsNullOrEmpty(extraOptions))
			{
				var response = await _jsonDownloader.DownloadAsync<FriendsIdsResponse>(
					String.Format(
						Url,
						"friends.get",
						String.Format("user_id={0}", id)),
					AccessToken
					);

				if (response != null &&
				    response.Response.Count != 0)
				{
					var users = new List<User>((int)response.Response.Count);
					users.AddRange(response.Response.FriendsIds.Select(userId => new User() {Id = userId}));

					return users;
				}
			}
			else
			{
				var response = await _jsonDownloader.DownloadAsync<FriendsUsersResponse>(
					String.Format(
						Url,
						"friends.get",
						String.Format("user_id={0}&{1}", id, extraOptions)));

				if (response != null &&
				    response.Response.Count != 0)
				{
					return response.Response.Friends;
				}
			}

			return new List<User>();
		}

		public async Task<List<Post>> GetWallPostsAsync(int id, uint count, uint offset)
		{
			var response = await _jsonDownloader.DownloadAsync<PostsResponse>(
				String.Format(
					Url,
					"wall.get",
					String.Format("owner_id={0}&Count={1}&offset={2}", id, count, offset)),
				AccessToken
				);

			if (response != null &&
			    response.Response != null)
				return response.Response.Posts;

			return new List<Post>();
		}

		public async Task<DateTime> GetServerTimeAsync()
		{
			var response = await _jsonDownloader.DownloadAsync<uint>(
				String.Format(
					Url,
					"utils.getServerTime",
					""));
			
			if (response != null)
				return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(response.Response);

			throw new Exception("Сервер недоступен");
		}

		public async Task<AccessToken> GetAccessTokenAsync(int clientId, string clientSecret, string redirectUri, string code)
		{
			var response = await _jsonDownloader.DownloadWithoutWrapperAsync<AccessToken>(
				String.Format(
					"https://oauth.vk.com/access_token?client_id={0}&client_secret={1}&redirect_uri={2}&code={3}",
					clientId,
					clientSecret,
					redirectUri,
					code)
				);

			if (response != null)
				return response;

			throw new Exception("Сервер недоступен");
		}
	}
}

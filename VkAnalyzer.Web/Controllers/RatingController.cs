﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using DatabaseAccess.Repositories;

using Domain.Domains;

using Microsoft.AspNet.Identity;

using NHibernate.Criterion;

using VkAnalyzer.Web.Infrastructure;
using VkAnalyzer.Web.Models;

namespace VkAnalyzer.Web.Controllers
{
	[Authorize]
    public class RatingController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Index(RatingViewModel rating)
		{
			if (!ModelState.IsValid) 
				return View();

			if (rating.CityId == 0)
				rating.CityId = null;

			var userRepository = IoC.Container.Resolve<IRepository<UserDomain>>();
			var criteria = new List<ICriterion>();

			if (rating.CityId != null)
				criteria.Add(Restrictions.Eq("City.Id", rating.CityId));

			var usersIds = userRepository.FilterBy(criteria)
			                             .Where(
				                             u => u.LoginUsers.Select(lu => lu.Id)
				                                   .Contains(int.Parse(User.Identity.GetUserId())))
			                             .Select(u => u.Id)
			                             .ToArray();

			var ratingResult = GetTop(usersIds, rating.FromDateTime, rating.ToDateTime, rating.Size, rating.SortColumn);
			ViewBag.Rating = ratingResult;

			return View("Rating", ratingResult);
		}

		private IEnumerable<PostViewModel> GetTop(uint[] usersIds, DateTime? fromDateTime, DateTime? toDateTime, int size, SortColumn sortColumn)
		{
			var postRep = IoC.Container.Resolve<IRepository<PostDomain>>();
			var posts = postRep.FilterBy(Restrictions.In("Owner.Id", usersIds));

			var snapshotRep = IoC.Container.Resolve<IRepository<PostSnapshotDomain>>();

			var res = new List<PostViewModel>();
			var owners = new Dictionary<uint, OwnerViewModel>();

			foreach (var post in posts)
			{
				var snap1 = fromDateTime != null
								? snapshotRep.FilterBy(p => p.Post.Id == post.Id && p.Date >= fromDateTime)
											 .OrderBy(p => p.Date)
											 .FirstOrDefault()
								: new PostSnapshotDomain()
								{
									CountComments = 0,
									CountReposts = 0,
									CountLikes = 0
								};
				var snap2 =
					snapshotRep.FilterBy(p => p.Post.Id == post.Id && p.Date <= (toDateTime.HasValue ? toDateTime : DateTime.Now))
					           .OrderByDescending(p => p.Date)
					           .FirstOrDefault();

				if (snap1 == null || snap2 == null) continue;

				var rating = new PostViewModel()
				{
					Id = post.Id,
					Text = post.Text,
					CountComments = (int)(snap2.CountComments - snap1.CountComments),
					CountReposts = (int)(snap2.CountReposts - snap1.CountReposts),
					CountLikes = (int)(snap2.CountLikes - snap1.CountLikes),
					Owner = GetOwner(post.Owner, owners)
				};

				res.Add(rating);
			}

			res = res.OrderByDescending(
				post =>
				{
					switch (sortColumn)
					{
						case SortColumn.ByComments:
							return post.CountComments;
						case SortColumn.ByReposts:
							return post.CountReposts;
						default:
							return post.CountLikes;
					}
				})
					 .Take(size)
					 .ToList();

			return res;
		}

		private OwnerViewModel GetOwner(UserDomain user, Dictionary<uint, OwnerViewModel> owners)
		{
			if (owners.ContainsKey(user.Id))
				return owners[user.Id];

			var owner = new OwnerViewModel()
			{
				Id = user.Id,
				FirstName = user.FirstName,
				LastName = user.LastName,
				Sex = user.Sex,
				City = user.City.Title
			};

			owners.Add(owner.Id, owner);

			return owner;
		}
	}
}
﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201512021737)]
	public class Mig009_AlterTableUser_SetCityIdAndCountryIdNullable : Migration
	{
		private const string TableName = "User";

		public override void Up()
		{
			Alter.Table(TableName).AlterColumn("countryId").AsInt32().Nullable();
			Alter.Table(TableName).AlterColumn("cityId").AsInt32().Nullable();
		}

		public override void Down()
		{
			Alter.Table(TableName).AlterColumn("cityId").AsInt32().NotNullable();
			Alter.Table(TableName).AlterColumn("countryId").AsInt32().NotNullable();
		}
	}
}

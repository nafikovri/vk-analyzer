﻿
using System.Collections.Generic;

namespace Domain.Domains
{
	public class CountryDomain
	{
		public virtual int Id { get; protected set; }
		public virtual string Title { get; set; }

		public virtual IList<UserDomain> Users { get; protected internal set; }


		public CountryDomain()
		{
			Users = new List<UserDomain>();
		}

		public virtual void AddUser(UserDomain user)
		{
			user.Country = this;
			Users.Add(user);
		}
	}
}

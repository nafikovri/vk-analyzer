﻿
using FluentMigrator;

namespace DatabaseMigrations.Schema
{
	[Migration(201511252228)]
	public class Mig006_CreateForeignKeys : Migration
	{
		public override void Up()
		{
			Create.ForeignKey("FK_User_Country")
			      .FromTable("User")
			      .ForeignColumn("countryId")
			      .ToTable("Country")
			      .PrimaryColumn("id");

			Create.ForeignKey("FK_User_City")
			      .FromTable("User")
			      .ForeignColumn("cityId")
			      .ToTable("City")
			      .PrimaryColumn("id");

			Create.ForeignKey("FK_PostSnapshot_Post")
			      .FromTable("PostSnapshot")
			      .ForeignColumn("postId")
			      .ToTable("Post")
			      .PrimaryColumn("id");

			Create.ForeignKey("FK_Post_User")
			      .FromTable("Post")
			      .ForeignColumn("ownerId")
			      .ToTable("User")
			      .PrimaryColumn("id");
		}

		public override void Down()
		{
			Delete.ForeignKey("FK_User_Country")
				  .OnTable("User");

			Delete.ForeignKey("FK_User_City")
				  .OnTable("User");

			Delete.ForeignKey("FK_PostSnapshot_Post")
				  .OnTable("PostSnapshot");

			Delete.ForeignKey("FK_Post_User")
			      .OnTable("Post");
		}
	}
}

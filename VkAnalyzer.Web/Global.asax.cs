﻿using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using VkAnalyzer.Web.Infrastructure;

namespace VkAnalyzer.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
	    protected void Application_Start()
	    {
			AutoMapping.RegisterMappings();

		    var container = IoC.Container;
			var castleControllerFactory = new CastleControllerFactory(container);
			ControllerBuilder.Current.SetControllerFactory(castleControllerFactory);

		    WebConstants.MqConnectionString = "host=localhost";

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}

﻿
using System;

namespace Domain.Domains
{
	public class PostSnapshotDomain
	{
		public virtual int Id { get; protected set; }
		public virtual PostDomain Post { get; protected set; }
		public virtual DateTime? Date { get; set; }				// "?" не позволит назначать дату по умолчанию. В БД поле NotNullable => исключение, если не поставить дату

		public virtual uint CountComments { get; set; }
		public virtual uint CountLikes { get; set; }
		public virtual uint CountReposts { get; set; }

		protected internal virtual void SetPost(PostDomain post)
		{
			Post = post;
		}
	}
}

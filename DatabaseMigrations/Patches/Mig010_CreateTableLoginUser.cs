﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201512091407)]
	public class Mig010_CreateTableLoginUser : Migration
	{
		private const string TableName = "LoginUser";

		public override void Up()
		{
			Create.Table(TableName)
			      .WithColumn("id").AsInt64().NotNullable().PrimaryKey("PK_" + TableName)
			      .WithColumn("firstName").AsString(30)
			      .WithColumn("lastName").AsString(40)
			      .WithColumn("accessToken").AsString(100);
		}

		public override void Down()
		{
			Delete.Table(TableName);
		}
	}
}

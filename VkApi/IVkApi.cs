﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using VkApi.Entities;

namespace VkApi
{
	public interface IVkApi
	{
		string AccessToken { get; set; }

		Task<User> GetUserAsync(uint id, string extraOptions = null);
		Task<List<User>> GetUsersAsync(uint[] ids, string extraOptions = null);
		Task<ResolveResult> ResolveScreenNameAsync(string screenName);
		Task<List<User>> GetFriendsAsync(uint id, string extraOptions = null);
		Task<List<Post>> GetWallPostsAsync(int id, uint count, uint offset);
		Task<DateTime> GetServerTimeAsync();

		Task<AccessToken> GetAccessTokenAsync(int clientId, string clientSecret, string redirectUri, string code);
	}

}

﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201511291958)]
	public class Mig008_AlterTablePostSnapshot : Migration
	{
		private const string TableName = "PostSnapshot";
		private const string ForeignKeyName = "FK_PostSnapshot_Post";

		public override void Up()
		{
			Delete.ForeignKey(ForeignKeyName).OnTable(TableName);

			Delete.PrimaryKey("PK_" + TableName).FromTable(TableName);

			Alter.Table(TableName).AddColumn("id").AsInt32().NotNullable();
			Create.PrimaryKey("PK_" + TableName).OnTable(TableName).Column("id");

			Create.ForeignKey(ForeignKeyName)
				  .FromTable(TableName)
				  .ForeignColumn("postId")
				  .ToTable("Post")
				  .PrimaryColumn("id");
		}

		public override void Down()
		{
			Delete.PrimaryKey("PK_" + TableName).FromTable(TableName);
			Delete.Column("id").FromTable(TableName);

			var keyColumns = new[] { "postId", "date" };
			Create.PrimaryKey("PK_" + TableName).OnTable(TableName).Columns(keyColumns);
		}
	}
}

﻿
using System;

using Newtonsoft.Json;

namespace VkApi.Converters
{
	public class ExpiresInConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var expiresIn = int.Parse(reader.Value.ToString());
			return DateTime.Now.AddSeconds(expiresIn);
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof (DateTime);
		}
	}
}

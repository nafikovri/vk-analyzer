﻿
using System;
using System.Collections.Generic;

namespace Domain.Domains
{
	public class SnaphotSequenceDomain
	{
		public virtual int Id { get; protected set; }
		public virtual DateTime Date { get; set; }

		public virtual IList<PostDomain> Posts { get; protected internal set; }

		public SnaphotSequenceDomain()
		{
			Posts = new List<PostDomain>();
		}

		public virtual void AddPost(PostDomain post)
		{
			post.SnaphotSequence = this;
			Posts.Add(post);
		}
	}
}

﻿
using System;

using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201601080403)]
	public class Mig021_AlterForeignKey_Post_PostSnapshot : Migration
	{
		private const string TableName_P = "Post";
		private const string TableName_PS = "PostSnapshot";

		public override void Up()
		{
			Delete.ForeignKey("FK_PostSnapshot_Post").OnTable(TableName_PS);

			Delete.PrimaryKey("PK_" + TableName_P).FromTable(TableName_P);
			Alter.Table(TableName_P).AlterColumn("id").AsString().NotNullable().PrimaryKey("PK_" + TableName_P);

			Alter.Table(TableName_PS).AlterColumn("postId").AsString().NotNullable();

			Create.ForeignKey("FK_PostSnapshot_Post")
				  .FromTable(TableName_PS)
				  .ForeignColumns("postId")
				  .ToTable(TableName_P)
				  .PrimaryColumns("id");
		}

		public override void Down()
		{
			throw new NotImplementedException();
		}
	}
}

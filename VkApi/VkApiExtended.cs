﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using VkApi.Entities;

namespace VkApi
{
	public class VkApiExtended
	{
		private readonly IVkApi _vkApi;

		public VkApiExtended(IVkApi vkApi)
		{
			_vkApi = vkApi;
		}

		#region Дополнительные методы над VK API

		public async Task<List<Post>> GetWallPostsAsync(int id, DateTime dateFrom)
		{
			var posts = new List<Post>();
			var prevPostsCount = 0;

			const uint count = 20;
			uint offset = 0;

			do
			{
				prevPostsCount = posts.Count;
				var newPosts = await _vkApi.GetWallPostsAsync(id, count, offset);
				posts.AddRange(newPosts.Where(post => post.Date >= dateFrom));

				offset += count;

				// выходим из цикла, когда  хотя бы один Post не попадает в заданный временной диапазон.
			} while ((posts.Count - prevPostsCount) == count);

			return posts;
		}

		public async Task<List<Post>> GetAllWallPostsAsync(int id)
		{
			const int count = 20;			// не более 100 постов за раз

			var result = new List<Post>();
			int prevPostsCount;
			uint offset = 0;

			do
			{
				prevPostsCount = result.Count;

				var posts = await _vkApi.GetWallPostsAsync(id, count, offset);
				offset += count;

				result.AddRange(posts);

				// выходим из цикла, когда получено менее "count" элементов.
			} while ((result.Count - prevPostsCount) == count);

			return result;
		}

		#endregion

		#region IVkApi Methods

		public async Task<User> GetUserAsync(uint id, string extraOptions = null)
		{
			return await _vkApi.GetUserAsync(id, extraOptions);
		}

		public async Task<List<User>> GetUsersAsync(uint[] ids, string extraOptions = null)
		{
			return await _vkApi.GetUsersAsync(ids, extraOptions);
		}

		public async Task<ResolveResult> ResolveScreenNameAsync(string screenName)
		{
			return await _vkApi.ResolveScreenNameAsync(screenName);
		}

		public async Task<List<User>> GetFriendsAsync(uint id, string extraOptions = null)
		{
			return await _vkApi.GetFriendsAsync(id, extraOptions);
		}

		public async Task<List<Post>> GetWallPostsAsync(int id, uint count, uint offset)
		{
			return await _vkApi.GetWallPostsAsync(id, count, offset);
		}

		public async Task<DateTime> GetServerTimeAsync()
		{
			return await _vkApi.GetServerTimeAsync();
		}

		#endregion

	}
}

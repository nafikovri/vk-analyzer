﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Moq;

using NUnit.Framework;

using VkAnalyzer;

using VkApi;
using VkApi.Entities;

namespace Tests
{
	[TestFixture]
	public class BasicLogicTest
	{

		private readonly IVkApi _vkApiPrototype;

		public BasicLogicTest()
		{
			// для тестирования ТОП-5 постов по лайкам
			var vkApiBuilder = new Mock<IVkApi>();

			#region GetUserAsync

			vkApiBuilder.Setup(api => api.GetUserAsync(1, null)).Returns(
				Task.FromResult(
					new User()
					{
						Id = 1,
						FirstName = "P",
						LastName = "D"
					}));

			#endregion

			#region GetUsersAsync

			vkApiBuilder.Setup(api => api.GetUsersAsync(new uint[] {1, 2, 3}, null)).Returns(
				Task.FromResult(
					new List<User>()
					{
						new User()
						{
							Id = 1,
							FirstName = "P",
							LastName = "D"
						},
						new User()
						{
							Id = 2,
							FirstName = "I",
							LastName = "F"
						},
						new User()
						{
							Id = 3,
							FirstName = "O",
							LastName = "L"
						}
					}));

			#endregion

			#region ResolseScreenName

			vkApiBuilder.Setup(api => api.ResolveScreenNameAsync("durov")).Returns(
				Task.FromResult(
					new ResolveResult()
					{
						Id = 1,
						Type = ResolveType.User
					}));
			vkApiBuilder.Setup(api => api.ResolveScreenNameAsync("group")).Returns(
				Task.FromResult(
					new ResolveResult()
					{
						Id = 10,
						Type = ResolveType.Group
					}));

			#endregion

			#region GetFriendsAsync

			vkApiBuilder.Setup(api => api.GetFriendsAsync(1, null)).Returns(
				Task.FromResult(
					new List<User>()
					{
						new User()
						{
							Id = 2,
							FirstName = "I",
							LastName = "F"
						},
						new User()
						{
							Id = 3,
							FirstName = "O",
							LastName = "L"
						}
					}));

			#endregion

			#region GetWallPostsAsync

			vkApiBuilder.Setup(api => api.GetWallPostsAsync(1, 20, 0)).Returns(
				Task.FromResult(
					new List<Post>()
					{
						new Post()
						{
							Id = 1,
							OwnerId = 1,
							LikesInfo = new LikesInfo()
							{
								Count = 5
							},
							Date = (new DateTime()).AddSeconds(1000 + (7 * 24 * 3600))
						},
						new Post()
						{
							Id = 2,
							OwnerId = 1,
							LikesInfo = new LikesInfo()
							{
								Count = 6
							},
							Date = (new DateTime()).AddSeconds(1000 + (7 * 24 * 3600))
						},
						new Post()
						{
							Id = 3,
							OwnerId = 1,
							LikesInfo = new LikesInfo()
							{
								Count = 7
							},
							Date = (new DateTime()).AddSeconds(1000 + (7 * 24 * 3600))
						}
					}));
			vkApiBuilder.Setup(api => api.GetWallPostsAsync(2, 20, 0)).Returns(
				Task.FromResult(
					new List<Post>()
					{
						new Post()
						{
							Id = 4,
							OwnerId = 2,
							LikesInfo = new LikesInfo()
							{
								Count = 8
							},
							Date = (new DateTime()).AddSeconds(1000 + (7 * 24 * 3600))
						},
						new Post()
						{
							Id = 5,
							OwnerId = 2,
							LikesInfo = new LikesInfo()
							{
								Count = 9
							},
							Date = (new DateTime()).AddSeconds(1000 + (7 * 24 * 3600))
						},
						new Post()
						{
							Id = 6,
							OwnerId = 2,
							LikesInfo = new LikesInfo()
							{
								Count = 10
							},
							Date = (new DateTime()).AddSeconds(3 * 24 * 3600) // этот пост не попадает в выборку за последнюю неделю
						}
					}));
			vkApiBuilder.Setup(api => api.GetWallPostsAsync(3, 20, 0)).Returns(
				Task.FromResult(
					new List<Post>()
					{
						new Post()
						{
							Id = 7,
							OwnerId = 3,
							LikesInfo = new LikesInfo()
							{
								Count = 1
							},
							Date = (new DateTime()).AddSeconds(1000 + (7 * 24 * 3600))
						},
						new Post()
						{
							Id = 8,
							OwnerId = 3,
							LikesInfo = new LikesInfo()
							{
								Count = 2
							},
							Date = (new DateTime()).AddSeconds(1000 + (7 * 24 * 3600))
						},
						new Post()
						{
							Id = 9,
							OwnerId = 3,
							LikesInfo = new LikesInfo()
							{
								Count = 3
							},
							Date = (new DateTime()).AddSeconds(1000 + (7 * 24 * 3600))
						}
					}));

			#endregion

			#region GetServerTimeAsync

			vkApiBuilder.Setup(api => api.GetServerTimeAsync()).Returns(
				Task.FromResult(
					(new DateTime()).AddSeconds(2 * 7 * 24 * 3600)		// 2 недели
					));

			#endregion

			_vkApiPrototype = vkApiBuilder.Object;
		}


		[Test]
		public async void GetFriendsTopPosts_CorrectData()
		{
			var baseAnalyzer = new BaseAnalyzer(new VkApiExtended(_vkApiPrototype));
			var posts = await baseAnalyzer.GetFriendsTopPosts("vk.com/durov", 7 * 24 * 3600, 5);

			Assert.AreEqual(posts.Count, 5);
			Assert.AreEqual(posts[0].LikesInfo.Count, 9);
			Assert.AreEqual(posts[4].LikesInfo.Count, 5);
		}

		[Test]
		public void GetFriendsTopPosts_NotUser()
		{
			var baseAnalyzer = new BaseAnalyzer(new VkApiExtended(_vkApiPrototype));

			Assert.Throws<Exception>(async () => await baseAnalyzer.GetFriendsTopPosts("vk.com/group", 7 * 24 * 3600, 5));
		}

	}
}

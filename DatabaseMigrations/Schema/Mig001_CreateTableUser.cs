﻿

using FluentMigrator;

namespace DatabaseMigrations.Schema
{
	[Migration(201511252131)]
	public class Mig001_CreateTableUser : Migration
	{
		private const string TableName = "User";

		public override void Up()
		{
			Create.Table(TableName)
			      .WithColumn("id").AsInt64().NotNullable()
			      .WithColumn("firstName").AsString(30).NotNullable()
			      .WithColumn("lastName").AsString(40)
			      .WithColumn("photoId").AsString(30)
			      .WithColumn("sex").AsByte().NotNullable()
			      .WithColumn("birthdate").AsDate()
			      .WithColumn("cityId").AsInt32().NotNullable()
			      .WithColumn("countryId").AsInt32().NotNullable();

			Create.PrimaryKey("PK_"+TableName).OnTable(TableName).Column("id");
		}

		public override void Down()
		{
			Delete.Table(TableName);
		}
	}
}

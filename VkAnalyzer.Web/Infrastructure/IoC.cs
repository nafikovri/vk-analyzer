﻿
using AspNet.Identity;

using Castle.Windsor;

using DatabaseAccess;

using Domain.Domains;

using VkApi;

namespace VkAnalyzer.Web.Infrastructure
{
	public class IoC
	{
		private const string DatabaseConnectionString = "server=localhost;user id=vkanalyzer;password=vkpassword;persistsecurityinfo=True;database=vk_analyzer";

		private static IWindsorContainer container;
		private static readonly object _lockObject = new object();

		public static IWindsorContainer Container {
			get
			{
				if (container == null)
				{
					lock (_lockObject)
					{
						if (container == null)
						{
							InitializeContainer();
						}
					}
				}

				return container;
			}
		}

		private static void InitializeContainer()
		{
			container = new WindsorContainer();
			container.Install(new WebInfrastructureInstaller());
			container.Install(new AccessDatabaseInstallerForWeb(DatabaseConnectionString, typeof(UserDomain).Assembly, typeof(IdentityUser).Assembly));
			container.Install(new VkApiInstaller());
		}
	}
}
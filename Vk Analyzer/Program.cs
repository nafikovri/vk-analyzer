﻿
using System;
using System.Linq;
using System.Threading;

using Castle.Windsor;

using DatabaseAccess;
using DatabaseAccess.Repositories;

using Domain;
using Domain.Domains;

using EasyNetQ;

using MQ.Messages;

namespace VkAnalyzer
{
	class Program
	{
		private static WindsorContainer _container;
		private const string DatabaseConnectionString = "server=localhost;user id=vkanalyzer;password=vkpassword;persistsecurityinfo=True;database=vk_analyzer";
		private const string MqConnectionString = "host=localhost";

		private static void Main(string[] args)
		{
			InitializeContainer();

			GlobalAutoMapping.RegisterMappings();

			while (true)
			{
				Console.Write("Generating tasks... ");
				GenerateTasks();
				Console.WriteLine("End");

				Console.WriteLine("Sleeping...");
				Thread.Sleep(20*60*1000);
			}
			
		}

		private static void GenerateTasks()
		{
			var userRepository = _container.Resolve<IRepository<UserDomain>>();

			var sequence = new SnaphotSequenceDomain
			{
				Date = DateTime.Now
			};

			var seqRepository = _container.Resolve<IRepository<SnaphotSequenceDomain>>();
			seqRepository.SaveOrUpdate(sequence);

			// получить id всех пользователей из БД
			var usersIds = userRepository.GetAll().Select(user => user.Id).ToList();

			using (var bus = RabbitHutch.CreateBus(MqConnectionString))
			{
				// раздаем задачу исполнителям
				foreach (var userId in usersIds)
				{
					var message = new UpdatePostsMessage(userId, sequence.Id);
					bus.Publish(message, MQ.MQConstants.UpdatePosts);

					//Console.WriteLine("Published:\t{0}", message.UserId);
				}
			}

			Console.Write("{0} users. ", usersIds.Count);
		}

		private static void InitializeContainer()
		{
			_container = new WindsorContainer();
			_container.Install(new AccessDatabaseInstaller(DatabaseConnectionString, typeof(UserDomain).Assembly));
		}
	}
}

﻿
using System;

using Castle.Core.Internal;

using Newtonsoft.Json;

using VkApi.Converters;

namespace VkApi.Entities
{
	public class AccessToken
	{
		[JsonProperty("access_token")]
		public string Token { get; set; }

		[JsonProperty("expires_in")]
		[JsonConverter(typeof (ExpiresInConverter))]
		public DateTime ExpiresIn { get; set; }

		[JsonProperty("user_id")]
		public uint UserId { get; set; }

		[JsonProperty("email")]
		public string Email { get; set; }

		[JsonProperty("error")]
		public string Error { get; set; }

		[JsonProperty("error_description")]
		public string ErrorDescription { get; set; }

		public bool IsCorrect { get { return !Token.IsNullOrEmpty(); } }
	}
}

﻿
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace VkApi
{
	public class VkApiInstaller : IWindsorInstaller
	{
		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			container.Register(
				Component.For<JsonDownloader>()
				         .DependsOn(Dependency.OnValue(typeof (int), 200))
				         .LifestyleSingleton(),

				Component.For<IVkApi>().ImplementedBy<VkApi>()
				         .LifestyleSingleton(),

				Component.For<VkApiExtended>()
				         .LifestyleSingleton()
				);
		}
	}
}

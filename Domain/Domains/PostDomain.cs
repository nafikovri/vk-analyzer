﻿
using System;
using System.Collections.Generic;

using VkApi.Entities;

namespace Domain.Domains
{
	public class PostDomain
	{
		public virtual string Id { get; protected set; }
		public virtual UserDomain Owner { get; protected internal set; }
		public virtual DateTime Date { get; set; }
		public virtual string Text { get; set; }
		public virtual PostType Type { get; set; }
		public virtual SnaphotSequenceDomain SnaphotSequence { get; protected internal set; }

		public virtual IList<PostSnapshotDomain> Snapshots { get; protected internal set; }
		
		public virtual IList<LoginUserDomain> LoginUsers { get; protected internal set; }


		public PostDomain()
		{
			Snapshots = new List<PostSnapshotDomain>();
			LoginUsers = new List<LoginUserDomain>();
		}

		public virtual void SetOwner(UserDomain owner)
		{
			owner.AddPost(this);
		}

		public virtual void SetSequence(SnaphotSequenceDomain sequence)
		{
			sequence.AddPost(this);
		}

		public virtual void AddSnapshot(PostSnapshotDomain snapshot)
		{
			snapshot.SetPost(this);
			Snapshots.Add(snapshot);
		}

		public override bool Equals(object obj)
		{
			var post = (obj as PostDomain);
			if (post == null) return false;

			return (Id.Equals(post.Id) && Owner.Equals(post.Owner));
		}

		public override int GetHashCode()
		{
			return (Id.GetHashCode() + Owner.Id.GetHashCode()).GetHashCode();
		}
	}
}

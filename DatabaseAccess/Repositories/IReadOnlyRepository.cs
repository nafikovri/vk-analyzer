﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using NHibernate;
using NHibernate.Criterion;

namespace DatabaseAccess.Repositories
{
	public interface IReadOnlyRepository<TEntity> where TEntity : class
	{
		TEntity FindBy(object id);
		IQueryable<TEntity> GetAll();
		TEntity FindBy(Expression<Func<TEntity, bool>> expression);
		IQueryable<TEntity> FilterBy(Expression<Func<TEntity, bool>> expression);
		IList<TEntity> FilterBy(ICriterion criterion);
		IList<TEntity> FilterBy(IEnumerable<ICriterion> criteria);

		IQueryOver<TEntity, TEntity> QueryOver();
		IQueryOver<TEntity, TEntity> QueryOver(Expression<Func<TEntity>> alias);
	}
}

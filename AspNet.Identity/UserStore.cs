﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

using FluentNHibernate.Utils;

using Microsoft.AspNet.Identity;

using NHibernate;
using NHibernate.Linq;
using NHibernate.Util;

namespace AspNet.Identity
{
	public class UserStore<TUser> : IUserPasswordStore<TUser>, IUserRoleStore<TUser>, IUserClaimStore<TUser>, IDisposable where TUser : IdentityUser
	{
		private bool _disposed;
		private readonly ISession _session;

		public UserStore(ISession session)
		{
			this._session = session;
		}

		#region IDiposable

		public bool ShouldDisposeSession { get; set; }

		public void Dispose()
		{
			this.Dispose(false);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing &&
				this._session != null &&
				this.ShouldDisposeSession)
			{
				this._session.Dispose();
			}

			this._disposed = true;
		}

		private void ThrowIfDisposed()
		{
			if (this._disposed)
			{
				throw new ObjectDisposedException(this.GetType().Name);
			}
		}

		#endregion


		#region CRUD

		public Task CreateAsync(TUser user)
		{
			ThrowIfDisposed();

			return Task.Run(
				() =>
				{
					ActionInTransaction(() => _session.Save(user));
					//_session.Flush();
					//_repository.Save(user);
				});
		}

		public Task<TUser> FindByIdAsync(string userId)
		{
			ThrowIfDisposed();

			return Task.Run(() => this._session.Get<TUser>(int.Parse(userId)));
			//return Task.Run(() => _repository.FindBy(int.Parse(userId)));
		}

		public Task<TUser> FindByNameAsync(string userName)
		{
			ThrowIfDisposed();

			return Task.Run(
				() => this._session.Query<TUser>()
				             .FirstOrDefault(user => user.LastName.ToUpper().Equals(userName.ToUpper())));

			//return Task.Run(() => _repository.FindBy(user => user.UserName.Equals(userName)));
		}

		public Task UpdateAsync(TUser user)
		{
			ThrowIfDisposed();

			return Task.Run(
				() =>
				{
					ActionInTransaction(() => _session.Update(user));
					//_session.Flush();
					//_repository.Update(user);
				});
		}

		public Task DeleteAsync(TUser user)
		{
			ThrowIfDisposed();

			return Task.Run(
				() =>
				{
					ActionInTransaction(() => _session.Delete(user));
					//_session.Flush();
					//_repository.Delete(user);
				});
		}


		private void ActionInTransaction(Action action)
		{
			using (var transaction = _session.BeginTransaction())
			{
				action();
				transaction.Commit();
			}
		}

		#endregion


		#region IUserPasswordStore

		public Task SetPasswordHashAsync(TUser user, string passwordHash)
		{
			throw new NotImplementedException();
		}

		public Task<string> GetPasswordHashAsync(TUser user)
		{
			throw new NotImplementedException();
		}

		public Task<bool> HasPasswordAsync(TUser user)
		{
			throw new NotImplementedException();
		}

		#endregion


		#region IUserRoleStore

		public Task AddToRoleAsync(TUser user, string roleName)
		{
			ThrowIfDisposed();

			return Task.Run(
				() =>
				{
					var role = this._session.Query<IdentityRole>()
					                  .SingleOrDefault(r => r.Name.ToUpper().Equals(roleName.ToUpper()));

					if (role == null)
						role = new IdentityRole(roleName);

					user.Roles.Add(role);
					role.Users.Add(user);

					UpdateAsync(user);
				});
		}

		public Task RemoveFromRoleAsync(TUser user, string roleName)
		{
			ThrowIfDisposed();
			
			return Task.Run(
				() =>
				{
					var role = user.Roles
					               .FirstOrDefault(r => r.Name == roleName);
					user.Roles.Remove(role);
					//role.Users.Remove(user);

					UpdateAsync(user);
				});
		}

		public Task<IList<string>> GetRolesAsync(TUser user)
		{
			ThrowIfDisposed();

			return Task.Run(
				() =>
				(IList<string>)user.Roles
				                   .Select(r => r.Name)
				                   .ToList()
				);
		}

		public Task<bool> IsInRoleAsync(TUser user, string roleName)
		{
			ThrowIfDisposed();

			return Task.Run(
				() =>
				roleName.ToUpper().In(user.Roles.Select(role => role.Name.ToUpper()).ToArray())
				);
		}

		#endregion


		#region IUserClaimStore

		public Task<IList<Claim>> GetClaimsAsync(TUser user)
		{
			ThrowIfDisposed();

			return Task.Run(
				() =>
				{
					var claims = new List<Claim>();
					foreach (var claim in user.Claims)
					{
						claims.Add(new Claim(claim.Type, claim.Value));
					}
					return (IList<Claim>)claims;
				});
		}

		public Task AddClaimAsync(TUser user, Claim claim)
		{
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException("user");
			if (claim == null)
				throw new ArgumentNullException("claim");

			return Task.Run(
				() =>
				{
					var newClaim = new IdentityUserClaim
					{
						User = user,
						Type = claim.Type,
						Value = claim.Value
					};
					user.Claims.Add(newClaim);

					UpdateAsync(user);
				});
		}

		public Task RemoveClaimAsync(TUser user, Claim claim)
		{
			ThrowIfDisposed();

			if (user == null)
				throw new ArgumentNullException("user");
			if (claim == null)
				throw new ArgumentNullException("claim");

			return Task.Run(
				() =>
				user.Claims
				    .Where(
					    userClaim =>
					    userClaim.Type == claim.Type && userClaim.Value == claim.Value)
				    .ForEach(userClaim => user.Claims.Remove(userClaim)));
		}

		#endregion

	}
}
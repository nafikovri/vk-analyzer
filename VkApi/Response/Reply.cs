﻿
using Newtonsoft.Json;

namespace VkApi.Response
{
	public class Reply<T>
	{
		[JsonProperty("response")]
		public T Response { get; set; }

		[JsonProperty("error")]
		public ErrorResponse Error { get; set; }
	}

	public class ErrorResponse
	{
		[JsonProperty("error_code")]
		public int Code { get; set; }

		[JsonProperty("error_msg")]
		public string Message { get; set; }
	}
}

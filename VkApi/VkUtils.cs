﻿using System;
using System.Text.RegularExpressions;

namespace VkApi
{
	public class VkUtils
	{
		public static string ParseUrl(string url)
		{
			var regex = new Regex(@"(https?://)?(www\.)?vk\.com/(?<name>[a-z][a-z0-9\.]+)\/?");

			var matches = regex.Matches(url.ToLower());
			if (matches.Count != 0)
				return matches[0].Groups["name"].Value;

			throw new Exception("Url is not correct.");
		}
	}
}
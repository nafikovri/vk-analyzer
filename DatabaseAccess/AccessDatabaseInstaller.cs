﻿using System.Reflection;

using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using DatabaseAccess.Repositories;

using NHibernate;

namespace DatabaseAccess
{
    public class AccessDatabaseInstaller : IWindsorInstaller
    {
	    private readonly string _connectionString;
		private readonly Assembly[] _assemblies;

	    public void Install(IWindsorContainer container, IConfigurationStore store)
	    {
		    container.Register(
			    Component.For(typeof (IRepository<>))
			             .ImplementedBy(typeof (Repository<>))
			             .LifestylePerThread(),

			    Component.For<ISession>().UsingFactoryMethod(
				    () =>
				    container.Resolve<NHibernateHelper>().SessionFactory.OpenSession())
			             .LifestylePerThread(),

			    Component.For<NHibernateHelper>()
			             .DependsOn(Dependency.OnValue("connectionString", _connectionString))
			             .DependsOn(Dependency.OnValue("assemblies", _assemblies))
			             .LifestyleSingleton()
			    );
	    }

	    public AccessDatabaseInstaller(string connectionString, params Assembly[] assemblies)
	    {
		    _connectionString = connectionString;
		    _assemblies = assemblies;
	    }
    }

	public class AccessDatabaseInstallerForWeb : IWindsorInstaller
	{
		private readonly string _connectionString;
		private readonly Assembly[] _assemblies;

		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			container.Register(
				Component.For(typeof(IRepository<>))
						 .ImplementedBy(typeof(Repository<>))
						 .LifestylePerWebRequest(),

				Component.For<ISession>().UsingFactoryMethod(
					() =>
					container.Resolve<NHibernateHelper>().SessionFactory.OpenSession())
					.LifestylePerWebRequest(),

				Component.For<NHibernateHelper>()
						 .DependsOn(Dependency.OnValue("connectionString", _connectionString))
						 .DependsOn(Dependency.OnValue("assemblies", _assemblies))
						 .LifestyleSingleton()
				);
		}

		public AccessDatabaseInstallerForWeb(string connectionString, params Assembly[] assemblies)
		{
			_connectionString = connectionString;
			_assemblies = assemblies;
		}
	}
}

﻿
using Domain.Domains;

using FluentNHibernate.Mapping;

namespace Domain.Maps
{
	public class CityMap : ClassMap<CityDomain>
	{
		public CityMap()
		{
			Table("city");

			Id(x => x.Id).Column("id").GeneratedBy.Assigned();
			Map(x => x.Title).Column("title");
			HasMany(x => x.Users).KeyColumn("cityId").LazyLoad().Inverse().Cascade.SaveUpdate();
		}
	}
}

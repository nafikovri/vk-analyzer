﻿
using Newtonsoft.Json;

namespace VkApi.Entities
{
	public class City
	{
		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("title")]
		public string Title { get; set; }
	}
}

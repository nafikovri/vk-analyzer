﻿
using Domain.Domains;

using FluentNHibernate.Mapping;

namespace Domain.Maps
{
	public class LoginUserMap : ClassMap<LoginUserDomain>
	{
		public LoginUserMap()
		{
			Table("loginuser");

			Id(x => x.Id).Column("id").GeneratedBy.Assigned();
			Map(x => x.FirstName).Column("firstName");
			Map(x => x.LastName).Column("lastName");
			Map(x => x.AccessToken).Column("accessToken");
			
			HasManyToMany(x => x.Users)
				.Table("loginuser_user")
				.ParentKeyColumn("loginUserId")
				.ChildKeyColumn("userId")
				.LazyLoad().Inverse().Cascade.SaveUpdate();

			//HasManyToMany(x => x.Posts)
			//	.Table("loginuser_post")
			//	.ParentKeyColumn("loginUserId")
			//	.ChildKeyColumn("postId")
			//	.LazyLoad().Inverse().Cascade.SaveUpdate();
		}
	}
}

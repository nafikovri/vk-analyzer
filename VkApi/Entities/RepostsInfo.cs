﻿
using Newtonsoft.Json;

namespace VkApi.Entities
{
	public class RepostsInfo
	{
		[JsonProperty("count")]
		public uint Count { get; set; }

		[JsonProperty("user_reposted")]
		public bool IsUserReposted { get; set; }
	}
}

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VkAnalyzer.Web.Startup))]
namespace VkAnalyzer.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

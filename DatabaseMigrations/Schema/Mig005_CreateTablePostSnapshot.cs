﻿
using FluentMigrator;

namespace DatabaseMigrations.Schema
{
	[Migration(201511252219)]
	public class Mig005_CreateTablePostSnapshot : Migration
	{
		private const string TableName = "PostSnapshot";

		public override void Up()
		{
			var keyColumns = new[] {"postId", "date"};

			Create.Table(TableName)
			      .WithColumn(keyColumns[0]).AsInt64().NotNullable()
			      .WithColumn(keyColumns[1]).AsDateTime().NotNullable()
			      .WithColumn("countComments").AsInt32().NotNullable()
			      .WithColumn("countLikes").AsInt32().NotNullable()
			      .WithColumn("countReposts").AsInt32().NotNullable();

			Create.PrimaryKey("PK_" + TableName).OnTable(TableName).Columns(keyColumns);
		}

		public override void Down()
		{
			Delete.Table(TableName);
		}
	}
}

﻿
using Domain.Domains;

using FluentNHibernate.Mapping;

using VkApi.Entities;

namespace Domain.Maps
{
	public class UserMap : ClassMap<UserDomain>
	{
		public UserMap()
		{
			Table("user");

			Id(x => x.Id).Column("id").GeneratedBy.Assigned();
			Map(x => x.FirstName).Column("firstName");
			Map(x => x.LastName).Column("lastName");
			Map(x => x.PhotoId).Column("photoId");
			Map(x => x.Sex).Column("sex").CustomType(typeof(Sex));
			Map(x => x.Birthday).Column("birthday");
			References(x => x.City).Column("cityId").Nullable().Cascade.SaveUpdate();
			References(x => x.Country).Column("countryId").Nullable().Cascade.SaveUpdate();

			HasMany(x => x.Posts).KeyColumn("ownerId").LazyLoad().Inverse().Cascade.SaveUpdate();

			HasManyToMany(x => x.LoginUsers)
				.Table("loginuser_user")
				.ParentKeyColumn("userId")
				.ChildKeyColumn("loginUserId")
				.LazyLoad().Cascade.SaveUpdate();
		}
	}
}

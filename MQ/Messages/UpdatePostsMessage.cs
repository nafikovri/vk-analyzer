﻿
namespace MQ.Messages
{
	public class UpdatePostsMessage
	{
		public uint UserId { get; private set; }
		public int SequenceId { get; private set; }

		public UpdatePostsMessage(uint userId, int sequenceId)
		{
			UserId = userId;
			SequenceId = sequenceId;
		}
	}
}

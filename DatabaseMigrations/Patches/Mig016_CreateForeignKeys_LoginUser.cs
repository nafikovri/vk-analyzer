﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201512091432)]
	public class Mig016_CreateForeignKeys_LoginUser : Migration
	{
		public override void Up()
		{
			Create.ForeignKey("FK_LoginUser_User_LoginUser")
			      .FromTable("LoginUser_User")
			      .ForeignColumn("loginUserId")
			      .ToTable("LoginUser")
			      .PrimaryColumn("id");

			Create.ForeignKey("FK_LoginUser_User_User")
				  .FromTable("LoginUser_User")
				  .ForeignColumn("userId")
				  .ToTable("User")
				  .PrimaryColumn("id");

			Create.ForeignKey("FK_LoginUser_Post_LoginUser")
				  .FromTable("LoginUser_Post")
				  .ForeignColumn("loginUserId")
				  .ToTable("LoginUser")
				  .PrimaryColumn("id");

			Create.ForeignKey("FK_LoginUser_Post_Post")
				  .FromTable("LoginUser_Post")
				  .ForeignColumn("postId")
				  .ToTable("Post")
				  .PrimaryColumn("id");
		}

		public override void Down()
		{
			Delete.ForeignKey("FK_LoginUser_User_LoginUser")
				  .OnTable("LoginUser_User");

			Delete.ForeignKey("FK_LoginUser_User_User")
				  .OnTable("LoginUser_User");

			Delete.ForeignKey("FK_LoginUser_Post_LoginUser")
				  .OnTable("LoginUser_Post");

			Delete.ForeignKey("FK_LoginUser_Post_Post")
				  .OnTable("LoginUser_Post");
		}
	}
}

﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201512121540)]
	public class Mig017_AlterTableLoginUser_AddColumnUsername :  Migration
	{
		private const string TableName = "LoginUser";

		public override void Up()
		{
			Alter.Table(TableName)
			     .AddColumn("username").AsString(50).NotNullable().Unique();
		}

		public override void Down()
		{
			Delete.Column("username").FromTable(TableName);
		}
	}
}

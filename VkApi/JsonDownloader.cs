﻿using System;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Castle.Core.Internal;

using Newtonsoft.Json;

using VkApi.Response;

namespace VkApi
{
	public class JsonDownloader
	{
		private readonly Encoding _encodingType = Encoding.UTF8;

		private readonly int _delay;
		private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1);

		public JsonDownloader(int delay)
		{
			this._delay = delay;
		}

		public async Task<Reply<T>> DownloadAsync<T>(string url, string accessToken = null)
		{
			this._semaphore.Wait();

			using (var wc = new WebClient(){Encoding = this._encodingType})
			{
				if (!accessToken.IsNullOrEmpty())
					url = AddAccessToken(url, accessToken);
				var sResponse = await wc.DownloadStringTaskAsync(url);
				var response = JsonConvert.DeserializeObject<Reply<T>>(sResponse);

				if (response.Error != null && 
					(response.Error.Code == 6 ||		// Too many requests per second
					response.Error.Code == 1))			// unknown error
				{
					Thread.Sleep(this._delay / 5);
					response = await DownloadWithoutDelayAsync<T>(url, 5);
				}

				new Thread(
					() =>
					{
						Thread.Sleep(this._delay);
						this._semaphore.Release();
					}).Start();

				return response;
			}
		}

		public async Task<T> DownloadWithoutWrapperAsync<T>(string url, string accessToken = null)
		{
			this._semaphore.Wait();

			using (var wc = new WebClient(){Encoding = this._encodingType})
			{
				if (!accessToken.IsNullOrEmpty())
					url = AddAccessToken(url, accessToken);
				var sResponse = await wc.DownloadStringTaskAsync(url);
				var response = JsonConvert.DeserializeObject<T>(sResponse);

				new Thread(
					() =>
					{
						Thread.Sleep(this._delay);
						this._semaphore.Release();
					}).Start();

				return response;
			}
		}

		private static string AddAccessToken(string url, string accessToken)
		{
			return String.Format("{0}&access_token={1}", url, accessToken);
		}

		private async Task<Reply<T>> DownloadWithoutDelayAsync<T>(string url, int countRetries)
		{
			using (var wc = new WebClient(){Encoding = this._encodingType})
			{
				var sResponse = await wc.DownloadStringTaskAsync(url);
				var response = JsonConvert.DeserializeObject<Reply<T>>(sResponse);

				if (response.Error != null &&
					(response.Error.Code == 6 || response.Error.Code == 1) &&
					countRetries > 0) 
				{
					response = await this.DownloadWithoutDelayAsync<T>(url, countRetries-1);
				}

				return response;
			}
		}
	}
}

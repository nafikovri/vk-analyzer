﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

using DatabaseAccess.Repositories;

using Domain.Domains;

using VkAnalyzer.Web.Infrastructure;

using VkApi.Entities;

namespace VkAnalyzer.Web.Models
{
	public class RatingViewModel
	{
		[Display(Name = "Город")]
		public int? CityId { get; set; }
		[Display(Name = "Начало периода")]
		public DateTime? FromDateTime { get; set; }
		[Display(Name = "Конец периода")]
		public DateTime? ToDateTime { get; set; }
		[Required]
		[Display(Name = "Размер топа")]
		public int Size { get; set; }
		[Required]
		[Display(Name = "Поле сортировки")]
		public SortColumn SortColumn { get; set; }
	}

	public class OwnerViewModel
	{
		public uint Id { get; set; }
		[Display(Name = "Имя")]
		public string FirstName { get; set; }
		[Display(Name = "Фамилия")]
		public string LastName { get; set; }
		[Display(Name = "Пол")]
		public Sex Sex { get; set; }
		[Display(Name = "Город")]
		public string City { get; set; }
	}

	public class PostViewModel
	{
		public string Id { get; set; }
		[Display(Name = "Содержание")]
		public string Text { get; set; }

		[Display(Name = "Владелец")]
		public OwnerViewModel Owner { get; set; }

		[Display(Name = "Комментарии")]
		public int CountComments { get; set; }
		[Display(Name = "Репосты")]
		public int CountReposts { get; set; }
		[Display(Name = "Лайки")]
		public int CountLikes { get; set; }
	}

	public enum SortColumn
	{
		ByComments,
		ByReposts,
		ByLikes
	}


	public class RatingUtils
	{
		public static IEnumerable<SelectListItem> GetSelectListItemsForSortColumn()
		{
			var result = new List<SelectListItem>();

			var item = new SelectListItem
			{
				Value = SortColumn.ByComments.ToString(),
				Text = "По комментариям"
			};
			result.Add(item);

			item = new SelectListItem
			{
				Value = SortColumn.ByReposts.ToString(),
				Text = "По репостам"
			};
			result.Add(item);

			item = new SelectListItem
			{
				Value = SortColumn.ByLikes.ToString(),
				Text = "По лайкам"
			};
			result.Add(item);

			return result;
		}

		public static IEnumerable<SelectListItem> GetSelectListItemsForCities(int loginUserId)
		{
			var rep = IoC.Container.Resolve<IRepository<UserDomain>>();

			var cities =
				rep.GetAll().Where(u => u.LoginUsers.Select(lu => lu.Id).Contains(loginUserId)).Select(u => u.City).Distinct().ToList();

			var result = new List<SelectListItem>();

			result.Add(new SelectListItem()
			{
				Text = "Все",
				Value = 0.ToString()
			});

			foreach (var city in cities)
			{
				var item = new SelectListItem()
				{
					Text = city.Title,
					Value = city.Id.ToString()
				};

				result.Add(item);
			}

			return result;
		}
	}
}
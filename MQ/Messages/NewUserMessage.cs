﻿using System;

namespace MQ.Messages
{
	public class NewUserMessage
	{
		public string Url { get; private set; }
		public AddingType AddingType { get; private set; }
		public int OwnerId { get; private set; }

		public NewUserMessage(string url, AddingType addingType, int ownerId)
		{
			Url = url;
			AddingType = addingType;
			OwnerId = ownerId;
		}
	}

	[Flags]
	public enum AddingType
	{
		UserOnly = 1,
		FriendsOnly = 2,
		UserAndFriends = 3
	}
}
﻿
using System;
using System.Collections.Generic;

using VkApi.Entities;

namespace Domain.Domains
{
	public class UserDomain
	{
		public virtual uint Id { get; protected set; }
		public virtual string FirstName { get; set; }
		public virtual string LastName { get; set; }
		public virtual string PhotoId { get; set; }
		public virtual Sex Sex { get; set; }
		public virtual DateTime Birthday { get; set; }
		public virtual CityDomain City { get; protected internal set; }
		public virtual CountryDomain Country { get; protected internal set; }
		public virtual string HomeTown { get; set; }

		public virtual IList<PostDomain> Posts { get; protected set; }

		public virtual IList<LoginUserDomain> LoginUsers { get; protected internal set; }


		public UserDomain()
		{
			Posts = new List<PostDomain>();
			LoginUsers = new List<LoginUserDomain>();
		}

		public virtual void AddPost(PostDomain post)
		{
			post.Owner = this;
			Posts.Add(post);
		}
		public virtual void AddPosts(IEnumerable<PostDomain> posts)
		{
			foreach (var post in posts)
			{
				AddPost(post);
			}
		}
		public virtual void SetCity(CityDomain city)
		{
			City = city;
			city.Users.Add(this);
		}
		public virtual void SetCountry(CountryDomain country)
		{
			Country = country;
			country.Users.Add(this);
		}
	}
}

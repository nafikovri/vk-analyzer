﻿
using Domain.Domains;

using FluentNHibernate.Mapping;

namespace Domain.Maps
{
	public class CountryMap : ClassMap<CountryDomain>
	{
		public CountryMap()
		{
			Table("country");

			Id(x => x.Id).Column("id").GeneratedBy.Assigned();
			Map(x => x.Title).Column("title");
			HasMany(x => x.Users).KeyColumn("countryId").LazyLoad().Inverse().Cascade.SaveUpdate();
		}
	}
}

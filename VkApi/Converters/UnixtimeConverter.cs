﻿using System;

using Newtonsoft.Json;

namespace VkApi.Converters
{
	public class UnixtimeConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var unixtime = uint.Parse(reader.Value.ToString());
			return new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(unixtime);
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof (uint);
		}
	}
}

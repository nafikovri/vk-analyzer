﻿using System.Collections.Generic;

using FluentNHibernate.Mapping;

using Microsoft.AspNet.Identity;

namespace AspNet.Identity
{
	public class IdentityUser : IUser
	{
		public virtual string Id							// не будет использоваться в БД, т.к. в БД нужно число
		{
			get { return this.DbId.ToString(); }
			protected set { this.DbId = int.Parse(value); }
		}
		
		public virtual int DbId { get; protected set; }		// будет использоваться в БД

		public virtual string UserName { get; set; }

		public virtual string FirstName { get; set; }

		public virtual string LastName { get; set; }

		public virtual string AccessToken { get; set; }


		public virtual IList<IdentityRole> Roles { get; protected set; }

		public virtual IList<IdentityUserClaim> Claims { get; protected set; }


		public IdentityUser()
		{
			this.Roles = new List<IdentityRole>();
			this.Claims = new List<IdentityUserClaim>();
		}

		public IdentityUser(string userName)
			: this()
		{
			this.UserName = userName;
		}
	}

	public class IdentityUserMap : ClassMap<IdentityUser>
	{
		public IdentityUserMap()
		{
			this.Table("loginuser");

			this.Id(x => x.DbId).Column("id").GeneratedBy.Assigned();
			Map(x => x.UserName).Column("username");
			Map(x => x.FirstName).Column("firstName");
			Map(x => x.LastName).Column("lastName");
			Map(x => x.AccessToken).Column("accessToken");

			HasManyToMany(x => x.Roles)
				.Table("loginuser_role")
				.ParentKeyColumn("loginUserId")
				.ChildKeyColumn("roleId")
				.LazyLoad().Cascade.SaveUpdate();

			HasMany(x => x.Claims)
				.KeyColumn("userId")
				.LazyLoad().Inverse().Cascade.SaveUpdate();
		}
	}
}

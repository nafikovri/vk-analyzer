﻿
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

using Microsoft.AspNet.Identity;

using VkAnalyzer.Web.Controllers;

using AspNet.Identity;

namespace VkAnalyzer.Web.Infrastructure
{
	public class WebInfrastructureInstaller : IWindsorInstaller
	{
		public void Install(IWindsorContainer container, IConfigurationStore store)
		{
			container.Register(
				// регистрация всех контроллеров
				Classes.FromThisAssembly().InSameNamespaceAs<HomeController>().LifestylePerWebRequest(),

				// далее для AccountController'а
				Component.For<UserManager<IdentityUser>>()
				         .OnCreate(
					         manager => ((UserValidator<IdentityUser>)manager.UserValidator).AllowOnlyAlphanumericUserNames = false)
				         .LifestylePerWebRequest(),

				Component.For<IUserStore<IdentityUser>>()
				         .ImplementedBy<UserStore<IdentityUser>>()
				         .LifestylePerWebRequest()
				);
		}
	}
}
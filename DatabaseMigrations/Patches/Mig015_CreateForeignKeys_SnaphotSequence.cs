﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201512091423)]
	public class Mig015_CreateForeignKeys_SnaphotSequence : Migration
	{
		public override void Up()
		{
			Create.ForeignKey("FK_Post_SnaphotSequence")
			      .FromTable("Post")
			      .ForeignColumn("sequenceId")
			      .ToTable("SnaphotSequence")
			      .PrimaryColumn("id");
		}

		public override void Down()
		{
			Delete.ForeignKey("FK_Post_SnaphotSequence")
			      .OnTable("Post");
		}
	}
}

﻿using System.Collections.Generic;

using Newtonsoft.Json;

using VkApi.Entities;

namespace VkApi.Response
{
	public abstract class FriendsResponse
	{
		[JsonProperty("count")]
		public uint Count { get; set; }
	}

	public class FriendsIdsResponse : FriendsResponse
	{
		[JsonProperty("items")]
		public List<uint> FriendsIds { get; set; }
	}

	public class FriendsUsersResponse : FriendsResponse
	{
		[JsonProperty("items")]
		public List<User> Friends { get; set; }
	}
}

﻿using System;
using System.Linq;
using System.Web.Mvc;

using Castle.Core.Internal;

using DatabaseAccess.Repositories;

using Domain.Domains;

using EasyNetQ;

using Microsoft.AspNet.Identity;

using MQ.Messages;

using VkAnalyzer.Web.Infrastructure;
using VkAnalyzer.Web.Models;

using VkApi;

namespace VkAnalyzer.Web.Controllers
{
	[Authorize]
    public class TargetController : Controller
    {
        public ActionResult Index()
        {
	        var userRepository = IoC.Container.Resolve<IRepository<UserDomain>>();
	        var targets = userRepository.GetAll().Where(u => u.LoginUsers.Select(lu => lu.Id).Contains(int.Parse(User.Identity.GetUserId())));

            return View(targets);
        }

	    public ActionResult Add()
	    {
		    return View("AddTarget");
	    }

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Add(NewTargetViewModel newTarget)
		{
			if (newTarget == null)
				return RedirectToAction("Add");				// todo: errors

			if (ModelState.IsValid)
			{
				try
				{
					if (VkUtils.ParseUrl(newTarget.Url).IsNullOrEmpty())
						return RedirectToAction("Add");
				}
				catch (Exception e)
				{
					// todo: errors
					return RedirectToAction("Add");
				}

				// послать сообщение
				using (var bus = RabbitHutch.CreateBus(WebConstants.MqConnectionString))
				{
					var message = new NewUserMessage(newTarget.Url, newTarget.AddingType, int.Parse(User.Identity.GetUserId()));
					bus.Publish(message, MQ.MQConstants.NewUser);
				}

				// todo: added successfully
				return RedirectToAction("Index");
			}
			else
				return RedirectToAction("Add");				// todo: errors
		}

	    public ActionResult Delete(int id)
		{
			var loginUserRepository = IoC.Container.Resolve<IRepository<LoginUserDomain>>();
			var loginUser = loginUserRepository.FindBy(int.Parse(User.Identity.GetUserId()));

			var target = loginUser.Users.FirstOrDefault(t => t.Id == id);
			if (target != null)
			{
				// удалить связку
				loginUser.RemoveUser(target);

				// если нет других наблюдателей, удалить цель (+ посты и снимки)
				if (target.LoginUsers.Count == 0)
				{
					var snapshotRepository = IoC.Container.Resolve<IRepository<PostSnapshotDomain>>();
					var postRepository = IoC.Container.Resolve<IRepository<PostDomain>>();
					var userRepository = IoC.Container.Resolve<IRepository<UserDomain>>();

					var posts = target.Posts;
					var snapshots = posts.SelectMany(post => post.Snapshots);

					snapshotRepository.Delete(snapshots);
					postRepository.Delete(posts);
					userRepository.Delete(target);
				}
			}

			return this.RedirectToAction("Index");
		}

    }
}

﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201512091422)]
	public class Mig014_AlterTablePost_AddSequenceColumn : Migration
	{
		public override void Up()
		{
			Alter.Table("Post")
			     .AddColumn("sequenceId").AsInt32().NotNullable();
		}

		public override void Down()
		{
			Delete.Column("sequenceId")
			      .FromTable("Post");
		}
	}
}

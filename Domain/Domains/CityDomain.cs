﻿
using System.Collections.Generic;

namespace Domain.Domains
{
	public class CityDomain
	{
		public virtual int Id { get; protected set; }
		public virtual string Title { get; set; }

		public virtual IList<UserDomain> Users { get; protected internal set; }


		public CityDomain()
		{
			Users = new List<UserDomain>();
		}

		public virtual void AddUser(UserDomain user)
		{
			user.City = this;
			Users.Add(user);
		}
	}
}

﻿
using System;

using Castle.Core.Internal;

namespace VkAnalyzer.Web.Infrastructure
{
	public class WebConstants
	{
		private static string _mqConnectionString;

		public static string MqConnectionString
		{
			get { return _mqConnectionString; }

			set
			{
				if (_mqConnectionString.IsNullOrEmpty())
					_mqConnectionString = value;
				else
					throw new Exception("Value already setted");
			}
		}
	}
}
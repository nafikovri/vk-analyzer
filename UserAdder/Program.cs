﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AutoMapper;

using Castle.Windsor;

using DatabaseAccess;
using DatabaseAccess.Repositories;

using Domain;
using Domain.Domains;

using EasyNetQ;

using MQ.Messages;

using NHibernate.Criterion;

using VkApi;
using VkApi.Entities;

namespace UserAdder
{
	class Program
	{
		private static IWindsorContainer _container;
		private const string MqConnectionString = "host=localhost";
		private const string DatabaseConnectionString = "server=localhost;user id=vkanalyzer;password=vkpassword;persistsecurityinfo=True;database=vk_analyzer";
		private const string ExtraOptions = "fields=sex,bdate,city,country,photo_id,home_town";

		private static void Main(string[] args)
		{
			InitializeContainer();

			GlobalAutoMapping.RegisterMappings();

			// постоянно получаем ссылки на пользователей из очереди и добавляем информацию о них в БД
			var bus = RabbitHutch.CreateBus(MqConnectionString);
			bus.Subscribe<NewUserMessage>(
				MQ.MQConstants.NewUser,
				AddNewUsers
				);
		}

		private static void InitializeContainer()
		{
			_container = new WindsorContainer();
			_container.Install(new AccessDatabaseInstaller(DatabaseConnectionString, typeof(UserDomain).Assembly));
			_container.Install(new VkApiInstaller());
		}

		private static void AddNewUsers(NewUserMessage message)
		{
			try						// это временно
			{
				var vkApi = _container.Resolve<IVkApi>();

				var screenName = VkUtils.ParseUrl(message.Url);

				Console.Write("Resolving screen name... ");
				var resolveScreenNameAsync = vkApi.ResolveScreenNameAsync(screenName);
				resolveScreenNameAsync.Wait();
				var resolveResult = resolveScreenNameAsync.Result;
				Console.WriteLine("End");

				var addedUsers = new List<UserDomain>();

				Console.Write("Loading users... ");
				if (resolveResult.Type == ResolveType.User)
				{
					var addFromUserInfoAsync = AddFromUserInfoAsync(resolveResult.Id, message.AddingType);
					addedUsers = addFromUserInfoAsync;
				}
				else if (resolveResult.Type == ResolveType.Group)
				{
					var addFromGroupInfoAsync = AddFromGroupInfoAsync(resolveResult.Id);
					addedUsers = addFromGroupInfoAsync;
				}
				Console.WriteLine("End");

				Console.Write("Updating... ");
				// убираем те цели, которые уже связаны с заданным наблюдателем
				addedUsers = addedUsers.Where(user => !user.LoginUsers.Select(lu => lu.Id).Contains(message.OwnerId))
				                       .ToList();

				// добавляем каждой цели нового наблюдателя
				var loginUserReposotory = _container.Resolve<IRepository<LoginUserDomain>>();
				var owner = loginUserReposotory.FindBy(message.OwnerId);
				foreach (var user in addedUsers)
				{
					user.LoginUsers.Add(owner);
				}
				_container.Resolve<IRepository<UserDomain>>().Update(addedUsers);
				Console.WriteLine("End");

				Console.WriteLine("Added {0} targets", addedUsers.Count);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
		}

		private static List<UserDomain> AddFromUserInfoAsync(uint userId, AddingType addingType)
		{
			var vkApi = _container.Resolve<IVkApi>();

			// получить множество пользователей из ВК
			var users = new List<User>();

			if (addingType.HasFlag(AddingType.UserOnly))
			{
				var userAsync = vkApi.GetUserAsync(userId, ExtraOptions);
				userAsync.Wait();
				users.Add(userAsync.Result);
			}

			if (addingType.HasFlag(AddingType.FriendsOnly))
			{
				var friendsAsync = vkApi.GetFriendsAsync(userId, ExtraOptions);
				friendsAsync.Wait();
				users.AddRange(friendsAsync.Result);
			}

			return AddUsers(users);
		}

		private static List<UserDomain> AddFromGroupInfoAsync(uint groupId)
		{
			throw new System.NotImplementedException();
		}

		private static List<UserDomain> AddUsers(List<User> users)
		{
			var userRepository = _container.Resolve<IRepository<UserDomain>>();

			var addedUsers = new List<UserDomain>();

			if (users.Count != 0)
			{
				// добавляем пользователей, которые уже есть в БД, в результирующий список
				var usersId = users.Select(user => user.Id).ToArray();
				var usersInDb = userRepository.FilterBy(Restrictions.In("Id", usersId)).ToList();

				addedUsers.AddRange(usersInDb);

				var domainCountries = new Dictionary<int, CountryDomain>();
				var domainCities = new Dictionary<int, CityDomain>();
				var domainUsers = new List<UserDomain>();

				// исключить пользователей, которые уже имеются в БД
				var usersInDbIds = usersInDb.Select(u => u.Id);
				users = users.FindAll(u => !usersInDbIds.Contains(u.Id));

				// преобразовать пользователей в формат приложения
				// и задать им города и страны
				foreach (var user in users)
				{
					var domainUser = Mapper.Map<User, UserDomain>(user);

					// создать/получить и назначить пользователю город
					if (user.City != null)
					{
						var domainCity = GetOrCreateCity(user.City, domainCities);
						domainUser.SetCity(domainCity);
					}

					// создать/получить и назначить пользователю страну
					if (user.Country != null)
					{
						var domainCountry = GetOrCreateCountry(user.Country, domainCountries);
						domainUser.SetCountry(domainCountry);
					}

					domainUsers.Add(domainUser); // добавляем пользователя в список
				}

				//var cityRep = _container.Resolve<IRepository<CityDomain>>();
				//var countryRep = _container.Resolve<IRepository<CountryDomain>>();
				//cityRep.SaveOrUpdate(domainCities.Values);
				//countryRep.SaveOrUpdate(domainCountries.Values);

				userRepository.Save(domainUsers);

				addedUsers.AddRange(domainUsers);
			}

			return addedUsers;
		}

		private static CityDomain GetOrCreateCity(City city, IDictionary<int, CityDomain> domainCities)
		{
			if (!domainCities.ContainsKey(city.Id))
			{
				var cityRepository = _container.Resolve<IRepository<CityDomain>>();
				var domainCity = cityRepository.FindBy(city.Id) ?? Mapper.Map<City, CityDomain>(city);
				domainCities.Add(city.Id, domainCity);
			}

			return domainCities[city.Id];
		}

		private static CountryDomain GetOrCreateCountry(Country country, IDictionary<int, CountryDomain> domainCountries)
		{
			if (!domainCountries.ContainsKey(country.Id))
			{
				var countryRepository = _container.Resolve<IRepository<CountryDomain>>();
				var domainCity = countryRepository.FindBy(country.Id) ?? Mapper.Map<Country, CountryDomain>(country);
				domainCountries.Add(country.Id, domainCity);
			}

			return domainCountries[country.Id];
		}
	}
}

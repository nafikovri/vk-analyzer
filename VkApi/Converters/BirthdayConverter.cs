﻿using System;

using Newtonsoft.Json;

namespace VkApi.Converters
{
	public class BirthdayConverter : JsonConverter
	{
		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			throw new NotImplementedException();
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var sBirthday = reader.Value.ToString();
			var dateComponents = sBirthday.Split('.');

			return new DateTime(
				dateComponents.Length >= 3 ? int.Parse(dateComponents[2]) : 1,
				int.Parse(dateComponents[1]),
				int.Parse(dateComponents[0]));
		}

		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(DateTime);
		}
	}
}
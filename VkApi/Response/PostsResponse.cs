﻿using System.Collections.Generic;

using VkApi.Entities;

using Newtonsoft.Json;

namespace VkApi.Response
{
	public class PostsResponse
	{
		[JsonProperty("count")]
		public uint Count { get; set; }

		[JsonProperty("items")]
		public List<Post> Posts { get; set; }
	}
}

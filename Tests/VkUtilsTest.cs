﻿using System;

using NUnit.Framework;

using VkApi;

namespace Tests
{
	[TestFixture]
	class VkUtilsTest
	{
		[Test]
		public void ParseUrl_Correct()
		{
			Assert.AreEqual(VkUtils.ParseUrl(@"https://vk.com/id123/"), "id123");
			Assert.AreEqual(VkUtils.ParseUrl(@"https://vk.com/name/"), "name");
			Assert.AreEqual(VkUtils.ParseUrl(@"Https://www.vk.com/test/"), "test");
			Assert.AreEqual(VkUtils.ParseUrl(@"Https://vk.com/test"), "test");
			Assert.AreEqual(VkUtils.ParseUrl(@"www.vk.com/test"), "test");
			Assert.AreEqual(VkUtils.ParseUrl(@"vk.com/test"), "test");
		}

		[Test]
		public void ParseUrl_Incorrect()
		{
			Assert.Throws<Exception>(() => VkUtils.ParseUrl(@"https://vk.com/123/"));
			Assert.Throws<Exception>(() => VkUtils.ParseUrl(@"https://vkontakte.com/id123/"));
		}

	}
}

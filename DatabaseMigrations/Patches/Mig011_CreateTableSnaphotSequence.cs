﻿
using FluentMigrator;

namespace DatabaseMigrations.Patches
{
	[Migration(201512091413)]
	public class Mig011_CreateTableSnaphotSequence : Migration
	{
		private const string TableName = "SnaphotSequence";

		public override void Up()
		{
			Create.Table(TableName)
			      .WithColumn("id").AsInt32().NotNullable().PrimaryKey("PK_" + TableName)
			      .WithColumn("date").AsDateTime().NotNullable();
		}

		public override void Down()
		{
			Delete.Table(TableName);
		}
	}
}

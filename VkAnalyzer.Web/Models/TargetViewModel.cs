﻿
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

using MQ.Messages;

namespace VkAnalyzer.Web.Models
{
	public class TargetViewModel
	{
	}

	public class NewTargetViewModel
	{
		[Required]
		[Display(Name = "Ссылка")]
		public string Url { get; set; }

		[Display(Name="Тип добавления")]
		public AddingType AddingType { get; set; }
	}

	public class TargetUtils
	{
		public static IEnumerable<SelectListItem> GetSelectListItemsForAddingType()
		{
			var result = new List<SelectListItem>();

			var item = new SelectListItem
			{
				Value = AddingType.UserOnly.ToString(),
				Text = "Только пользователь"
			};
			result.Add(item);

			item = new SelectListItem
			{
				Value = AddingType.FriendsOnly.ToString(),
				Text = "Только друзья"
			};
			result.Add(item);

			item = new SelectListItem
			{
				Value = AddingType.UserAndFriends.ToString(),
				Text = "Пользователь и друзья"
			};
			result.Add(item);

			return result;
		}
	}
}
﻿
using Domain.Domains;

using FluentNHibernate.Mapping;

using VkApi.Entities;

namespace Domain.Maps
{
	public class PostMap : ClassMap<PostDomain>
	{
		public PostMap()
		{
			Table("post");

			Id(x => x.Id).Column("id").GeneratedBy.Assigned();
			References(x => x.Owner).Column("ownerId").Not.Nullable();
			Map(x => x.Date).Column("date");
			Map(x => x.Text).Column("text");
			Map(x => x.Type).Column("type").CustomType(typeof(PostType));
			HasMany(x => x.Snapshots).KeyColumn("postId").LazyLoad().Inverse().Cascade.SaveUpdate();
			References(x => x.SnaphotSequence).Column("sequenceId").Not.Nullable();

			//HasManyToMany(x => x.LoginUsers)
			//	.Table("loginuser_post")
			//	.ParentKeyColumn("postId")
			//	.ChildKeyColumn("loginUserId")
			//	.LazyLoad().Cascade.SaveUpdate();
		}
	}
}

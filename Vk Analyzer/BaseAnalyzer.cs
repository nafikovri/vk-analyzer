﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using VkApi;
using VkApi.Entities;

namespace VkAnalyzer
{
	public class BaseAnalyzer
	{
		private readonly VkApiExtended _vkApi;

		public BaseAnalyzer(VkApiExtended vkApi)
		{
			_vkApi = vkApi;
		}

		/// <summary>
		/// Возвращает посты друзей отсортированные по количеству лайков
		/// </summary>
		/// <param name="userUrl">Ссылка на пользователя</param>
		/// <param name="lastPeriod">Длительность периода в секундах</param>
		/// <param name="count">Количество возвращаемых записей</param>
		/// <returns></returns>
		public async Task<List<Post>> GetFriendsTopPosts(string userUrl, uint lastPeriod, int count)
		{
			var name = VkUtils.ParseUrl(userUrl);
			var resolveResult = await _vkApi.ResolveScreenNameAsync(name);

			if (resolveResult.Type != ResolveType.User)
			{
				throw new Exception("Object is not a User");
			}

			var user = await _vkApi.GetUserAsync(resolveResult.Id);

			var friends = await _vkApi.GetFriendsAsync(user.Id);
			friends.Add(user);

			var currentTime = await _vkApi.GetServerTimeAsync();
			var someTimeAgo = currentTime.AddSeconds(-lastPeriod);

			var posts = new List<Post>();
			foreach (var friend in friends)
				posts.AddRange(await _vkApi.GetWallPostsAsync((int)friend.Id, someTimeAgo));

			posts = posts.OrderByDescending(p => p.LikesInfo.Count).Take(count).ToList();
			return posts;
		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

using AutoMapper;

using Domain.Domains;

using VkApi.Entities;

namespace Domain
{
	public static class GlobalAutoMapping
	{
		/// <summary>
		/// Достаточно вызывать один раз при запуске приложения.
		/// </summary>
		public static void RegisterMappings()
		{
			Mapper.CreateMap<User, UserDomain>()
			      .ForMember("City", p => p.Ignore())
			      .ForMember("Country", p => p.Ignore());

			Mapper.CreateMap<User, LoginUserDomain>();

			Mapper.CreateMap<City, CityDomain>();

			Mapper.CreateMap<Country, CountryDomain>();

			Mapper.CreateMap<Post, PostDomain>()
			      .ForMember("Id", p => p.MapFrom(post => post.Id + "_" + post.OwnerId))
			      .ForMember("Owner", p => p.Ignore())
			      .AfterMap(
				      (post, postDomain) => postDomain.AddSnapshot(
					      new PostSnapshotDomain()
					      {
						      CountComments = post.CommentsInfo.Count,
						      CountLikes = post.LikesInfo.Count,
						      CountReposts = post.RepostsInfo.Count
					      }));

			Mapper.CreateMap<Post, PostSnapshotDomain>()
			      .ForMember("Id", p => p.Ignore())
			      .ForMember("CountComments", p => p.MapFrom(post => post.CommentsInfo.Count))
			      .ForMember("CountLikes", p => p.MapFrom(post => post.CommentsInfo.Count))
			      .ForMember("CountReposts", p => p.MapFrom(post => post.RepostsInfo.Count));
		}

		public static List<PostDomain> Map(List<Post> posts, DateTime dateTime)
		{
			var domainPosts = posts.Select(Mapper.Map<Post, PostDomain>).ToList();
			domainPosts.ForEach(p=>p.Snapshots[0].Date = dateTime);					// назначение даты/времени снимка
			return domainPosts;
		}

		public static List<UserDomain> Map(List<User> users)
		{
			return users.Select(Mapper.Map<User, UserDomain>).ToList();
		} 
	}
}

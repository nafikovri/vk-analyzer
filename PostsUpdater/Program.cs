﻿
using System;
using System.Collections.Generic;
using System.Linq;

using Castle.Core.Internal;
using Castle.Windsor;

using DatabaseAccess;
using DatabaseAccess.Repositories;

using Domain;
using Domain.Domains;

using EasyNetQ;

using FluentNHibernate.Utils;

using MQ.Messages;

using VkApi;

namespace PostsUpdater
{
	class Program
	{
		private static IWindsorContainer _container;
		private const string DatabaseConnectionString = "server=localhost;user id=vkanalyzer;password=vkpassword;persistsecurityinfo=True;database=vk_analyzer";
		private const string MqConnectionString = "host=localhost";

		static void Main(string[] args)
		{
			InitializeContainer();

			GlobalAutoMapping.RegisterMappings();

			// постоянно получаем ссылки на пользователей из очереди и добавляем информацию о них в БД
			var bus = RabbitHutch.CreateBus(MqConnectionString);
			
			bus.Subscribe<UpdatePostsMessage>(MQ.MQConstants.UpdatePosts, UpdatePosts);
		}

		private static void InitializeContainer()
		{
			_container = new WindsorContainer();
			_container.Install(new AccessDatabaseInstaller(DatabaseConnectionString, typeof(UserDomain).Assembly));
			_container.Install(new VkApiInstaller());
		}
		private static void UpdatePosts(UpdatePostsMessage message)
		{
			Console.WriteLine("Recieved:\t{0}", message.UserId);

			try									// временно
			{
				var vkApi = _container.Resolve<VkApiExtended>();

				var userRepository = _container.Resolve<IRepository<UserDomain>>();
				var user = userRepository.FindBy(message.UserId);

				var seqRepository = _container.Resolve<IRepository<SnaphotSequenceDomain>>();
				var sequence = seqRepository.FindBy(message.SequenceId);

				// получить все посты пользователя из VK
				// и назначить дату/время snapshot'ам
				var allWallPostsAsync = vkApi.GetAllWallPostsAsync((int)user.Id);
				allWallPostsAsync.Wait();
				var posts = GlobalAutoMapping.Map(allWallPostsAsync.Result, sequence.Date);

				// добавить новую информацию о старых постах
				var newPostsIds = posts.Select(p => p.Id).ToArray();				// чтобы не пытаться обновить удаленные посты
				var oldPosts = user.Posts
				                   .Where(p => p.Id.In(newPostsIds));
				foreach (var post in oldPosts)
				{
					var postDomain = posts.Single(p=>p.Id == post.Id);
					post.AddSnapshot(postDomain.Snapshots[0]);
				}

				// добавить в коллекцию новые посты
				var oldPostsIds = user.Posts.Select(p => p.Id).ToArray();			// чтобы добавлять только новые посты
				var newPosts = posts.Where(p => !p.Id.In(oldPostsIds)).ToList();

				foreach (var post in newPosts)
					post.SetSequence(sequence);

				user.AddPosts(newPosts);

				// сохранить посты в БД
				userRepository.Update(user);
			}
			catch (Exception e)
			{
				Console.WriteLine(e);
			}
		}

	}
}

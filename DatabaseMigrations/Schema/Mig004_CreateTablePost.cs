﻿
using FluentMigrator;

namespace DatabaseMigrations.Schema
{
	[Migration(201511252201)]
	public class Mig004_CreateTablePost : Migration
	{
		private const string TableName = "Post";

		public override void Up()
		{
			var keyColumns = new[] {"id", "ownerId"};

			Create.Table(TableName)
			      .WithColumn(keyColumns[0]).AsInt64().NotNullable()
			      .WithColumn(keyColumns[1]).AsInt64().NotNullable()
			      .WithColumn("sourceId").AsInt64().NotNullable()
			      .WithColumn("date").AsDateTime().NotNullable()
			      .WithColumn("text").AsString().NotNullable()
			      .WithColumn("type").AsByte().NotNullable();

			Create.PrimaryKey("PK_" + TableName).OnTable(TableName).Columns(keyColumns);
		}

		public override void Down()
		{
			Delete.Table(TableName);
		}
	}
}

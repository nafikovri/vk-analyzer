﻿
using Domain.Domains;

using FluentNHibernate.Mapping;

namespace Domain.Maps
{
	class SnapshotSequenceMap : ClassMap<SnaphotSequenceDomain>
	{
		public SnapshotSequenceMap()
		{
			Table("snaphotsequence");

			Id(x => x.Id).Column("id").GeneratedBy.Increment();
			Map(x => x.Date).Column("date");
			HasMany(x => x.Posts).KeyColumn("sequenceId").LazyLoad().Inverse().Cascade.SaveUpdate();
		}
	}
}
